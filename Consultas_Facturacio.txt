(: Nombres de los clientes con una factura más grande de 500€ :)
for $c in doc('Escritorio/XML/XQuery/facturacio.xml')//factura
where $c /total > 500
return $c//raosocial/nom

(: Recibos dónde se ha vendido cerdito de chocolate :)
sum(for $c in doc('Escritorio/XML/XQuery/facturacio.xml')//factura
where $c //producte/descripcio[contains(.,'Porquet de xocolata')]
return count($c/rebut))

(: Dinero que se ha ingresado vendiendo un determinado producto :)
for $c in doc('Escritorio/XML/XQuery/facturacio.xml')//producte
where $c //descripcio[contains(.,'blanca')]
return $c/preuunitari * $c/quantitat

(: Recibos con 3 o más artículos vendidos :)
for $c in doc('Escritorio/XML/XQuery/facturacio.xml')//rebut
let $quant := 3
where $c /linies/producte/quantitat >= $quant
return $c/@numero

(: Lista del total de las facturas, ordenada de mayor a menor :)
for $c in doc('Escritorio/XML/XQuery/facturacio.xml')//factura
order by number($c/total) descending
return ($c/@numero,$c/total)


